/*
Las siguientes dependencias se usan para marcar un origen, realizar pipes (transformaciones), y mandar a un destino.
Son las siguientes:
  gulp-html-minifier2: se usa para reducir espacio en un archivo html (eliminar espacios en blanco).
  gulp-less y gulp-csso: se usa para reducir espacio en archivo css y también para optimitzar el código. (Ejemplo: juntar variables diferentes con mismos estilos)
  gulp-uglify: se usa para reducir espacio en un archivo js.

Dicho esto, hay 5 tareas declaradas en total:
  tarea html, tarea css, tarea js, tarea img (solo mueve archivos).
  Por último, la tarea default, que ejecuta todas las anteriores.
 */
var gulp = require('gulp');
var less = require('gulp-less');
var minifyCSS = require('gulp-csso');
var htmlmin = require('gulp-html-minifier2');
var uglify = require('gulp-uglify');

gulp.task('html', function(){
  return gulp.src('*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('../build'));
});

gulp.task('css', function(){
  return gulp.src('css/*.css')
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(gulp.dest('../build/css'));
});

gulp.task('js', function(){
  return gulp.src('js/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('../build/js'));
});

gulp.task('img', function(){
  return gulp.src('media/images/*')
    .pipe(gulp.dest('../build/media/images'));
});

gulp.task('default', [ 'html', 'css', 'js', 'img' ]);
