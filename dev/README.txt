Antes de empezar con la explicación de la práctica comentar que el archivo .js no tiene error si no un warning por la función setTimeout(function).

-Explicación página web:
La página web de Dungeon Master consta de:
  4 botones: empezar, guardar, recuperar, borrar partida.
  3 secciones en la misma fila: Minimapa, Juego, Equipo & Características


-Explicación funcionamiento del juego:
Al comenzar partida, se descarga el slot "nueva" de la api y llena las variables con la información por defecto.
El usuario podrá elegir su nombre y sexo y guardarlo en la variable "player" del javascript.
El jugador solo ve una casilla por delante en la dirección donde mira.
Si hay un enemigo enfrente, empezará a luchar. Cada pocos milisegundos se efectua un ataque.
Cuando ataca el jugador, el enemigo muestra una imagen diferente con la vida restada o si lo ha esquivado.
Cuando ataca el enemigo, se muestra durante pocos milisegundos la vida afectada del jugador en diferente color.
Lo mismo pasa con la experiencia, se muestra en amarillo cuando se ha sumado la experiencia del enemigo derrotado.
Para la lucha no hay habilidades especiales, todo es automático y depende de las características del jugador y del enemigo.
Añadir que durante una lucha no se puede escapar, hay que finalizarla, hay que prevenir y saber si podrás matar al enemigo antes de enfrentarte a el.
(En la función lucharEnemigo() del javascript está explicado como se hacen los ataques)

En cuanto a los objetos, el jugador verá el objeto si está una casilla por delante de el, y si avanza podrá cogerlo.
El objeto se guarda en la mochila del jugador y además se muestra como botón en la sección "equipo & características" apartado "objetos". El botón usa función onclick.
Si se usa el objeto, puede desaparecer si es "pocion", o no si es "llave".
En el caso de que los objetos sean "garrote" o "escudo", se añadiran a la manoderecha y manoizquierda respectivamente.
Arma de ataque sólo es para mano derecha, y arma de defensa sólo para izquierda.
En caso de que se quiera sacar objeto de la mano se vuelve a clicar en el botón situado en "manoderecha" o "manoizquierda" y se añade a la mochila de nuevo.


-Funcionalidades nuevas:
  Minimapa con brújula: Se muestra una matriz 10x10 con lo que contiene cada casilla y la posición del jugador.
                        También se muestra una brújula que apunta a la dirección actual del jugador.
                        La aguja de la brújula se transforma a los grados dependiendo de la dirección.
Comentar que se ha añadido a la sección "equipo & características" la experiencia, manoderecha y manoizquierda.
También se ha hecho la página de manera responsive, no tanto como para teléfono móvil pero si para pantallas más anchas o un poco más estrechas.


-Explicación GULP:
El archivo gulpfile.js y package.json estan situados en la carpeta "dev".
En el archivo package.json se puede ver que dependencias tenemos instaladas.
Estas dependencias se usan en el archivo gulpfile.js para marcar un origen, realizar pipes (transformaciones), y mandar a un destino.
Son las siguientes:
  gulp-html-minifier2: se usa para reducir espacio en un archivo html (eliminar espacios en blanco).
  gulp-less y gulp-csso: se usa para reducir espacio en archivo css y también para optimitzar el código. (Ejemplo: juntar variables diferentes con mismos estilos)
  gulp-uglify: se usa para reducir espacio en un archivo js.

Dicho esto, hay 5 tareas declaradas en total:
  tarea html, tarea css, tarea js, tarea img (solo mueve archivos).
  Por último, la tarea default, que ejecuta todas las anteriores.
Para usar GULP situar en la carpeta "dev" mediante terminal y ejecutar comando "gulp default".
De esta manera se generará en la misma carpeta donde se situa "dev", una carpeta llamada "build" con los archivos transformados.
