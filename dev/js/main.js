var partida_empezada = false;
//Variable para interrumpir la huida de un jugador si está en un combate
var jugador_atacando = false;

var api_url = "http://puigpedros.salleurl.edu/pwi/pac4/partida.php?token=6c414a13-e258-4c99-a88f-432a5be1033e";

/* Inicializar el juego */
function iniciarJuego() {
  //Ajustamos el canvas al total de pixeles de anchura de la seccion padre
  var canvas = document.querySelector('canvas');
  fitToContainer(canvas);

  function fitToContainer(canvas){
    var width = document.getElementById('visor').offsetWidth;
    var height = document.getElementById('visor').offsetHeight;
    // Make it visually fill the positioned parent
    canvas.style.width ='100%';
    canvas.style.height='100%';
    // ...then set the internal size to match
    canvas.width  = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
  }
  pintaImagen("start.png", 0, 0);
}

/* Convierte lo que hay en el mapa en un archivo de imagen */
function mapaToImg(x, y) {
  switch (mapa[x][y]) {
    case "step":
      return "dungeon_step.png";
    case "wall":
      return "dungeon_wall.png";
    case "door":
      return "dungeon_door.png";
    case "enemy":
      return "dungeon_enemy_lifefull.png";
    case "pocion":
      return "dungeon_object_potion.png";
    case "llave":
      return "dungeon_object_key.png";
  }
}

/*
  Función que se ejecuta en la función do_get_prompt
  una vez se ha obtenido la configuración de un slot: "nueva", "1" o "2".
 */
function empezarPartida() {
  partida_empezada = true;
  inicializarDatosUsuario();
  creaMinimapa();
  pintaBrujula();
  if (player.estadoPartida.nivel < 0) {
    muestraDelante();
  } else {
    pintaImagen("finish.png", 0, 0);
  }
}

/*
  Función que pinta lo que está delante del jugador
 */
function muestraDelante() {
  //mostramos lo que ve el jugador enfrente segun su dirección
  switch (player.estadoPartida.direccion) {
    case 0:
      pintaPosicion(player.estadoPartida.x - 1, player.estadoPartida.y);
      break;
    case 1:
      pintaPosicion(player.estadoPartida.x + 1, player.estadoPartida.y);
      break;
    case 2:
      pintaPosicion(player.estadoPartida.x, player.estadoPartida.y + 1);
      break;
    case 3:
      pintaPosicion(player.estadoPartida.x, player.estadoPartida.y - 1);
      break;
  }
}

/*
  Función que pinta en matriz 10x10 (minimapa) lo que contiene cada casilla.
 */
function creaMinimapa() {
  var minimap = document.getElementById("map");
  minimap.innerHTML = "";
  for (i = 0; i < mapa.length; i++) {
    minimap.innerHTML += "<div class='row' id='row"+i+"'></div>";
    for (j = 0; j < mapa[i].length; j++) {
      document.getElementById("row"+i).innerHTML += "<div class='col' id='col"+i+j+"'></div>";
      if (player.estadoPartida.nivel == 0) {
        document.getElementById("col"+i+j).style.background = "#a6a6a6 url('./media/images/minimap_grass.png') no-repeat center";
      } else if (mapa[i][j] == "door") {
        document.getElementById("col"+i+j).style.background = "#a6a6a6 url('./media/images/minimap_door.png') no-repeat center";
      } else if (mapa[i][j] == "step") {
        document.getElementById("col"+i+j).style.background = "#a6a6a6 url('./media/images/minimap_floor.png') no-repeat center";
      } else if (mapa[i][j] == "enemy") {
        document.getElementById("col"+i+j).style.background = "#a6a6a6 url('./media/images/minimap_enemy.png') no-repeat center";
      } else if (mapa[i][j] == "wall") {
        document.getElementById("col"+i+j).style.background = "#a6a6a6 url('./media/images/minimap_wall.png') no-repeat center";
      } else if (mapa[i][j] == "pocion") {
        document.getElementById("col"+i+j).style.background = "#a6a6a6 url('./media/images/minimap_potion.png') no-repeat center";
      } else if (mapa[i][j] == "llave") {
        document.getElementById("col"+i+j).style.background = "#a6a6a6 url('./media/images/minimap_key.png') no-repeat center";
      }
    }
  }
  document.getElementById("col"+player.estadoPartida.x+player.estadoPartida.y).style.background = "#a6a6a6 url('./media/images/minimap_player.png') no-repeat center";
}

/*
  Función equivalente a los controles del jugador

  Variables de entrada:
    direction: Puede tener los siguientes valores
      ahead: Avanzar una casilla hacia alante en la dirección del jugador.
      behind: Retroceder una casilla hacia atras.
      left: Girar 90 grados de la direccion del jugador hacia la izquierda
      right: Girar 90 grados de la direccion del jugador hacia la derecha
 */
function playerMove(direction) {
  //Si la partida no ha empezado o el jugador ataca o ya se pasó la mazmorra entonces no hace ningun movimiento.
  if (!partida_empezada) {
    alert("La partida no ha empezado");
    return;
  } else if (jugador_atacando) {
    alert("Escapar es de cobardes");
    return;
  } else if (player.estadoPartida.nivel == 0) {
    alert("Ya te has pasado el juego");
    return;
  }
  if (direction == "ahead" || direction == "behind") {
    var movement;
    if (direction == "ahead") {
      movement = 1;
    } else {
      movement = -1;
    }
    if (comprobarPuerta(movement) && !comprobarLlave()) {
      alert("No dispones de una llave para entrar");
      return;
    }
    if (!comprobarColisionPared(movement)) {
      switch (player.estadoPartida.direccion) {
        case 0:
          player.estadoPartida.x = player.estadoPartida.x - movement;
          pintaPosicion(player.estadoPartida.x - 1, player.estadoPartida.y);
          break;
        case 1:
          player.estadoPartida.x = player.estadoPartida.x + movement;
          pintaPosicion(player.estadoPartida.x + 1, player.estadoPartida.y);
          break;
        case 2:
          player.estadoPartida.y = player.estadoPartida.y + movement;
          pintaPosicion(player.estadoPartida.x, player.estadoPartida.y + 1);
          break;
        case 3:
          player.estadoPartida.y = player.estadoPartida.y - movement;
          pintaPosicion(player.estadoPartida.x, player.estadoPartida.y - 1);
          break;
      }
      if (mapa[player.estadoPartida.x][player.estadoPartida.y] == "door") {
        entrarPuerta();
      }
      creaMinimapa();
    } else {
      alert("Estás en una pared, no puedes avanzar");
    }
  } else if (direction == "left") {
    switch (player.estadoPartida.direccion) {
      case 0:
        player.estadoPartida.direccion = 3;
        pintaPosicion(player.estadoPartida.x, player.estadoPartida.y - 1);
        break;
      case 1:
        player.estadoPartida.direccion = 2;
        pintaPosicion(player.estadoPartida.x, player.estadoPartida.y + 1);
        break;
      case 2:
        player.estadoPartida.direccion = 0;
        pintaPosicion(player.estadoPartida.x - 1, player.estadoPartida.y);
        break;
      case 3:
        player.estadoPartida.direccion = 1;
        pintaPosicion(player.estadoPartida.x + 1, player.estadoPartida.y);
        break;
    }
  } else if (direction == "right") {
    switch (player.estadoPartida.direccion) {
      case 0:
        player.estadoPartida.direccion = 2;
        pintaPosicion(player.estadoPartida.x, player.estadoPartida.y + 1);
        break;
      case 1:
        player.estadoPartida.direccion = 3;
        pintaPosicion(player.estadoPartida.x, player.estadoPartida.y - 1);
        break;
      case 2:
        player.estadoPartida.direccion = 1;
        pintaPosicion(player.estadoPartida.x + 1, player.estadoPartida.y);
        break;
      case 3:
        player.estadoPartida.direccion = 0;
        pintaPosicion(player.estadoPartida.x - 1, player.estadoPartida.y);
        break;
    }
  }

  //Despues de cada movimiento se comprueba si hay objeto o si hay enemigo.
  //También pinta la nueva dirección de la aguja de la brújula.
  pintaBrujula();
  if (compruebaObjeto()) {
    var objeto = mapa[player.estadoPartida.x][player.estadoPartida.y];
    mapa[player.estadoPartida.x][player.estadoPartida.y] = "step";
    player.mochila.push(objeto);
    pintaObjetos();
  }
  if (comprobarEnemigo()) {
    lucharEnemigo();
  }
}

/*
  Función para pintar los objetos en la sección equipo del jugador
  Para cada objeto en la mochila del jugador se genera un botón que tiene onclick a la función usaObjeto(class)
 */
function pintaObjetos() {
  document.getElementById("objetos").innerHTML = "";
  for (i = 0; i < player.mochila.length; i++) {
    document.getElementById("objetos").innerHTML += " <button class='"+player.mochila[i]+"' onclick='usaObjeto(this.className)'>"+player.mochila[i]+"</button>";
  }

  document.getElementById("manoderecha").innerHTML = "";
  document.getElementById("manoizquierda").innerHTML = "";
  if (player.manoderecha.length > 0) {
    document.getElementById("manoderecha").innerHTML += " <button class='"+player.manoderecha+"' onclick='quitaObjeto(this.className)'>"+player.manoderecha+"</button>";
  }
  if(player.manoizquierda.length > 0) {
    document.getElementById("manoizquierda").innerHTML += " <button class='"+player.manoizquierda+"' onclick='quitaObjeto(this.className)'>"+player.manoizquierda+"</button>";
  }
}

/*
  Función que devuelve true si hay una pared donde el jugador tiene previsto moverse

  Variables de entrada:
    movement: Se usa para saber a donde es el siguiente movimiento y si en ese punto hay pared
      1: Valdrá 1 cuando se quiera avanzar en la dirección "ahead" (up)
      -1: Valdrá -1 cuando se quiera retroceder en la dirección "behind" (down)
 */
function comprobarColisionPared(movement) {
  switch (player.estadoPartida.direccion) {
    case 0:
      if (mapa[player.estadoPartida.x - movement][player.estadoPartida.y] == "wall") {
        return true;
      }
      break;
    case 1:
      if (mapa[player.estadoPartida.x + movement][player.estadoPartida.y] == "wall") {
        return true;
      }
      break;
    case 2:
      if (mapa[player.estadoPartida.x][player.estadoPartida.y + movement] == "wall") {
        return true;
      }
      break;
    case 3:
      if (mapa[player.estadoPartida.x][player.estadoPartida.y - movement] == "wall") {
        return true;
      }
      break;
  }
  return false;
}

/*
  Función que se encarga de pintar en la sección 'equipo & características' los datos del jugador al iniciar la partida
 */
function inicializarDatosUsuario() {
  document.getElementById("username").placeholder = player.nombre;
  document.getElementById("username").disabled = false;
  if (player.sexo === "Hombre") {
    document.getElementById('male_radio').checked = true;
  } else {
    document.getElementById('female_radio').checked = true;
  }
  document.getElementById('female_radio').disabled = false;
  document.getElementById('male_radio').disabled = false;
  document.getElementById("nivel").innerHTML = player.nivel;
  document.getElementById("xp").innerHTML = player.xp + "/" + calculaXp();
  document.getElementById("ataque").innerHTML = player.ataque;
  document.getElementById("defensa").innerHTML = player.defensa;
  document.getElementById("vida").innerHTML = "<span id='vida_actual'>" + player.vida + "</span>/<span id='vida_max'>" + calculaVida() + "</span>";
  //Si era una partida guardada tendrá objetos, se pintan
  pintaObjetos();
}

/*
  Función que se ejecuta cuando se clica en el botón 'Guarda' en la sección 'equipo & características'
  Pone el valor escrito en el nombre y el sexo a los atributos del objeto 'player'
 */
function guardaDatos() {
  if (!partida_empezada) {
    alert("Tienes que empezar una partida");
    return;
  }
  player.nombre = document.getElementById("username").value;
  if (document.getElementById('female_radio').checked) {
    player.sexo = "Mujer";
  } else {
    player.sexo = "Hombre";
  }
  alert("Nombre y sexo guardados con éxito");
}


/*
  Función que se encarga de comprobar si estamos frente a una puerta.

  Variable de entrada:
    movement: se usa porque se puede acceder a la puerta aunque el jugador la tenga detrás si se mueve hacia atrás.
      1: Valdrá 1 cuando se quiera avanzar en la dirección "ahead" (up)
      -1: Valdrá -1 cuando se quiera retroceder en la dirección "behind" (down)

  Return true si hay una puerta en la dirección donde el jugador quiere moverse
 */
function comprobarPuerta(movement) {
  if (player.estadoPartida.direccion == 0 && mapa[player.estadoPartida.x - movement][player.estadoPartida.y] == "door" ||
  player.estadoPartida.direccion == 1 && mapa[player.estadoPartida.x + movement][player.estadoPartida.y] == "door" ||
  player.estadoPartida.direccion == 2 && mapa[player.estadoPartida.x][player.estadoPartida.y + movement] == "door" ||
  player.estadoPartida.direccion == 3 && mapa[player.estadoPartida.x][player.estadoPartida.y - movement] == "door") {
    return true;
  }
  return false;
}

/*
  Función que se encarga de comprobar si disponemos de la llave en la mochila.
  Return true si se dispone de la llave.
 */
function comprobarLlave() {
  for (i = 0; i < player.mochila.length; i++) {
    if (player.mochila[i] == "llave") {
      return true;
    }
  }
  return false;
}

/*
  Función que comprueba la dirección del jugador y si delante de esa dirección está un enemigo (siguiente casilla)
  Return true si hay enemigo
 */
function comprobarEnemigo() {
  if (player.estadoPartida.direccion == 0 && mapa[player.estadoPartida.x - 1][player.estadoPartida.y] == "enemy" ||
  player.estadoPartida.direccion == 1 && mapa[player.estadoPartida.x + 1][player.estadoPartida.y] == "enemy" ||
  player.estadoPartida.direccion == 2 && mapa[player.estadoPartida.x][player.estadoPartida.y + 1] == "enemy" ||
  player.estadoPartida.direccion == 3 && mapa[player.estadoPartida.x][player.estadoPartida.y - 1] == "enemy") {
    return true;
  }
  return false;
}

/*
  Función que se ejecuta cuando el usuario está en la puerta (ha podido entrar ya que tenia llave)
  Se suma el nivel de la partida y pinta el nuevo mapa.
 */
function entrarPuerta() {
  player.estadoPartida.nivel++;
  if (player.estadoPartida.nivel < 0) {
    alert("Has conseguido pasar el nivel " + player.estadoPartida.nivel + " de la mazmorra. Suerte en el siguiente nivel");

    mapa = mapa2;

    //Se elimina la llave usada
    player.mochila.splice(player.mochila.indexOf("llave"), 1);

    pintaObjetos();
    muestraDelante();
  } else {
    alert("Enhorabuena, eres libre");
    pintaImagen("finish.png", 0, 0);
  }
}

/*
  Función que se ejecuta cuando el jugador está enfrente de un enemigo.
  Se realiza la lucha de la siguiente manera:
    Se calcula el numero de ataques total del combate en un bucle
    incrementando en uno hasta que el jugador o el enemigo tienen <= 0 vida
    Luego se ejecuta tantos setTimeout como numero de ataques.
    El setTimeout ejecuta una función pasados X milisegundos.
    Pues tanto en el turno del jugador como del enemigo se tardará el mismo tiempo y se visualizará el turno en tiempo real.
 */
function lucharEnemigo() {
  jugador_atacando = true;
  var turno = 0; //0: turno jugador, 1: turno enemigo
  var vida_enemigo = enemigo.vida;
  var vida_jugador = player.vida;
  var numeroAtaques = 0;

  //Se calcula el numero de ataques total que tendrá el combate entre el jugador y el enemigo
  while (vida_enemigo > 0 && vida_jugador > 0) {
    if (numeroAtaques % 2 == 0) {
      vida_enemigo = vida_enemigo - calculaDaño("jugador");
    } else {
      vida_jugador = vida_jugador - calculaDaño("enemigo");
    }
    numeroAtaques++;
  }
  vida_enemigo = enemigo.vida;
  var i = 0;

  //Se ejecuta tantos setTimeout como numero de ataques
  while (i < numeroAtaques) {

    setTimeout(function(){
      var hit;
      if (turno == 0) {
        document.getElementById("vida_actual").style.color = 'black';
        hit = calculaDaño("jugador");
        if (hit < 0) {
          hit = 0;
        }
        vida_enemigo = vida_enemigo - hit;
        if (vida_enemigo < 0) {
          vida_enemigo = 0;
        }
        var image = "dungeon_enemy_hit"+hit+"_life"+vida_enemigo+".png";
        pintaImagen(image, 0, 0);

        turno = 1;
      } else {
        hit = calculaDaño("enemigo");
        if (hit <= 0) {
          hit = 0;
          document.getElementById("vida_actual").style.color = 'blue';
        } else {
          document.getElementById("vida_actual").style.color = 'red';
          player.vida = player.vida - hit;
          document.getElementById("vida_actual").innerHTML -= hit;
        }


        if (player.vida <= 0) {
          partida_empezada = false;
          pintaImagen("gameover.png", 0, 0);
          return;
        } else {
          pintaImagen("dungeon_enemy.png", 0, 0);
        }

        turno = 0;
      }
    }, (i+1) * 500);

    i++;
  }

  //Finalizado el ultimo ataque si el jugador ha sobrevivido, se deberá eliminar el enemigo,
  //sumar experiencia, coger los objetos del enemigo y repintar el juego
  setTimeout(function(){
    if (player.vida > 0) {
      jugador_atacando = false;
      borraEnemigo();
      sumaExperiencia();
      añadeObjetosEnemigo();
      muestraDelante();
    }
  }, (i+1) * 500);


}

/*
  Función que se ejecuta una vez se ha eliminado al enemigo y se elimina del mapa poniendo suelo.
  Finalmente se ejecuta la función creaMinimapa() porque el enemigo tampoco debe estar ahí.
 */
function borraEnemigo() {
  switch(player.estadoPartida.direccion) {
    case 0:
      mapa[player.estadoPartida.x - 1][player.estadoPartida.y] = "step";
      break;
    case 1:
      mapa[player.estadoPartida.x + 1][player.estadoPartida.y] = "step";
      break;
    case 2:
      mapa[player.estadoPartida.x][player.estadoPartida.y + 1] = "step";
      break;
    case 3:
      mapa[player.estadoPartida.x][player.estadoPartida.y - 1] = "step";
      break;
  }
  creaMinimapa();
}

/*
  Función que calcula el daño del jugador o del enemigo.
  Valores variable de entrada:
    -personaje: Puede valer "jugador" o "enemigo"
 */
function calculaDaño(personaje) {
  var hit;
  if (personaje == "jugador") {
    if (player.manoderecha.length != 0) {
      hit = (player.ataque + objetos[player.manoderecha].ataque) - enemigo.defensa;
    } else {
      hit = player.ataque - enemigo.defensa;
    }
  } else if (personaje == "enemigo") {
    if (player.manoizquierda.length != 0) {
      hit = enemigo.ataque - (player.defensa + objetos[player.manoizquierda].defensa);
    } else {
      hit = enemigo.ataque - player.defensa;
    }
  }
  return hit;
}

/*
  Función que recorre el array de objetos del enemigo y los añade a la mochila del jugador.
  Finalmente pinta los objetos en la sección "equipo y características".
  Esta función se ejecuta una vez se ha eliminado a un enemigo.
 */
function añadeObjetosEnemigo() {
  for (i = 0; i < enemigo.objetos.length; i++) {
    player.mochila.push(enemigo.objetos[i]);
  }
  pintaObjetos();
}

/*
  Función que comprueba si el jugador está en la misma posición del mapa donde hay un objeto.
  Return true si hay objeto
  Return false si no hay objeto
 */
function compruebaObjeto() {
  var keyNames = Object.keys(objetos);
  for (var i in keyNames) {
      if (keyNames[i] == mapa[player.estadoPartida.x][player.estadoPartida.y]) {
        return true;
      }
  }
  return false;
}

/*
  Función que usa un objeto. Recibe la clase del objeto ya que el botón tiene atributo className
  Esta clase es el nombre del objeto y se busca en la mochila hasta encontrarlo.
 */
function usaObjeto(className) {
  for (i = 0; i < player.mochila.length; i++) {
    if (player.mochila[i] == className) {
      switch (player.mochila[i]) {
        case "pocion":
          document.getElementById("vida_actual").style.color = 'green';
          setTimeout(function(){
            document.getElementById("vida_actual").style.color = 'black';
          }, 2000);
          player.vida = player.vida + objetos.pocion.vida;
          var vida_maxima = calculaVida();
          if (player.vida > vida_maxima) {
            player.vida = vida_maxima;
          }
          player.mochila.splice(i, 1);
          document.getElementById("vida_actual").innerHTML = player.vida;
          pintaObjetos();
          break;
        case "garrote":
          if (player.manoderecha.length == 0) {
            player.manoderecha = "garrote";
            player.mochila.splice(i, 1);
            pintaObjetos();
            alert("Garrote añadido a la mano derecha");
          } else {
            alert("Ya tienes un garrote añadido en la mano derecha");
          }
          break;
        case "escudo":
        if (player.manoizquierda.length == 0) {
          player.manoizquierda = "escudo";
          player.mochila.splice(i, 1);
          pintaObjetos();
          alert("Escudo añadido a la mano izquierda");
        } else {
          alert("Ya tienes un escudo añadido en la mano izquierda");
        }
          break;
      }
      break;
    }
  }
}

/*
  Función que se encarga de quitar el objeto de una mano y devolverlo a la mochila del jugador
 */
function quitaObjeto(className) {
  if (player.manoderecha == className) {
    player.mochila.push(player.manoderecha);
    player.manoderecha = "";
    pintaObjetos();
  } else if (player.manoizquierda == className) {
    player.mochila.push(player.manoizquierda);
    player.manoizquierda = "";
    pintaObjetos();
  }
}

/*
  Función que suma experiencia al usuario.
 */
function sumaExperiencia() {
  player.xp = player.xp + enemigo.xp;

  //Pinta durante unos segundos la experiencia en amarillo para visualizar el cambio.
  document.getElementById("xp").style.color = "yellow";
  setTimeout(function(){
    document.getElementById("xp").style.color = "black";
  }, 2000);

  //Si la experiencia del jugador es mayor o igual a la calculada para el siguiente nivel entonces incrementa los niveles.
  if (player.xp >= calculaXp()) {
    player.nivel++;
    document.getElementById("nivel").innerHTML = player.nivel;
    player.defensa++;
    document.getElementById("defensa").innerHTML = player.defensa;
    if (player.nivel % 2 != 0) {
      player.ataque++;
      document.getElementById("ataque").innerHTML = player.ataque;
    }
    var vida_sumada = 10 * player.nivel;
    player.vida = player.vida + vida_sumada;

    document.getElementById("vida").innerHTML = "<span id='vida_actual'>" + player.vida + "</span>/<span id='vida_max'>" + calculaVida() + "</span>";

    alert("Has subido de nivel!");
  }

  //Se pinta en la sección "equipo y carac" la experiencia actual del jugador y la necesaria para el siguiente nivel
  document.getElementById("xp").innerHTML = player.xp + "/" + calculaXp();
}

/*
  Función que calcula la experiencia necesaria para alcanzar el siguiente nivel a partir del nivel actual del jugador
 */
function calculaXp() {
  var nivel_jugador = player.nivel;
  var experiencia_next_lvl = 0;
  while (nivel_jugador >= 1) {
    experiencia_next_lvl = experiencia_next_lvl + 10 * (nivel_jugador + 1);
    nivel_jugador--;
  }

  return experiencia_next_lvl;
}

/*
  Función que calcula la vida máxima que puede tener el jugador en su nivel actual
 */
function calculaVida() {
  var nivel_jugador = player.nivel;
  var vida_max = 0;
  while (nivel_jugador >= 1) {
    vida_max = vida_max + 10 * nivel_jugador;
    nivel_jugador--;
  }
  return vida_max;
}

function do_post(slot) {
  partida.mapa = mapa;
  partida.mapa2 = mapa2;
  partida.objetos = objetos;
  partida.enemigo = enemigo;
  partida.player = player;

  $.ajax({
   method: "POST",
   url: api_url+"&slot="+slot,
   data: { json: JSON.stringify(partida) }
  }).done(function(data) {
   alert("Partida guardada con exito");
  });
 }

function do_delete(slot) {
  $.ajax({
   method: "DELETE",
   url: api_url+"&slot="+slot
  }).done(function(data) {
   alert("Partida borrada");
  });
}

/*
  Función que hace un GET a la API y obtiene un json que es un array de strings con los slots que existen actualmente
 */
function do_get(action) {
  $.ajax({
   method: "GET",
   url: api_url,
   error: function() {
            alert("No se ha podido conectar con la API");
          }
  }).done(function(data) {
    switch (action) {
      case "guarda":
        guardarPartida(data);
        break;
      case "recupera":
        recuperarPartida(data);
        break;
      case "elimina":
        eliminarPartida(data);
        break;
    }
  });
}

/*
  Función que hace un GET a la API y obtiene el json del slot deseado "nueva", "1" o "2"
  Una vez se recibe los datos se llena las variables y se empieza la partida
 */
function do_get_prompt(slot) {
  $.ajax({
   method: "GET",
   url: api_url+"&slot="+slot,
   error: function() {
            alert("No se ha podido conectar con la API");
          }
  }).done(function(data) {
   partida = JSON.parse(data);
   mapa = partida.mapa;
   mapa2 = partida.mapa2;
   objetos = partida.objetos;
   enemigo = partida.enemigo;
   player = partida.player;
   empezarPartida();
  });
}

/*
  Función para guardar la partida.
  Recibe objeto data equivalente al array de strings (del json a partir de do_get()) que indica qué slots existen.
 */
function guardarPartida(data) {
  //Si no hay una partida empezada no se puede guardar.
  if (!partida_empezada) {
    alert("No se puede guardar partida si no hay una activa");
    return;
  }

  var jsonArray = JSON.parse(data);
  var slot;
  //Si solo hay un String en el array quiere decir que es el "nueva" y no hay ninguna partida guardada por el jugador.
  if (jsonArray.length == 1) {
    slot = prompt("No tienes ninguna partida guardada. Donde guardar? {1,2}", "");
    do_post(slot);
  } else if (jsonArray.length == 2) {
    if (jsonArray[1] == "1") {
      slot = confirm("Tienes partida guardada en el slot 1, guardar en el 2?");
      do_post("2");
    } else {
      slot = confirm("Tienes partida guardada en el slot 2, guardar en el 1?");
      do_post("1");
    }
  } else if (jsonArray.length == 3) {
    alert("Tienes ya dos partidas guardadas. Hay que borrar una");
  }
}

/*
  Función para recuperar una partida guardada.
 */
function recuperarPartida(data) {
  var jsonArray = JSON.parse(data);
  var slot;
  if (jsonArray.length == 1) {
    alert("No hay ninguna partida guardada");
  } else if (jsonArray.length == 2) {
    var answer = confirm("Tienes una partida guardada en el slot " + jsonArray[1] + ". ¿Quieres recuperar?");
    do_get_prompt(jsonArray[1]);
  } else if (jsonArray.length == 3) {
    slot = prompt("¿Qué partida quieres recuperar? {1,2}", "");
    do_get_prompt(slot);
  }
}

/*
  Función para eliminar una partida guardada.
 */
function eliminarPartida(data) {
  var jsonArray = JSON.parse(data);
  var slot;
  if (jsonArray.length == 1) {
    alert("No hay partidas guardadas para borrar");
  } else if (jsonArray.length == 2) {
    var answer = confirm("Tienes una partida guardada en el slot " + jsonArray[1] + ". ¿Quieres borrar?");
    do_delete(jsonArray[1]);
  } else if (jsonArray.length == 3) {
    slot = prompt("¿Qué partida quieres borrar? {1,2}", "");
    do_delete(slot);
  }
}

/*
  Función que pinta la brújula dependiendo de la dirección actual del jugador, entonces realiza transformación en grados a la aguja.
 */
function pintaBrujula() {
    switch (player.estadoPartida.direccion) {
      case 0:
        // Standard syntax
        document.getElementById("arrow").style.transform = "rotate(0deg)";
        break;
      case 1:
        // Standard syntax
        document.getElementById("arrow").style.transform = "rotate(180deg)";
        break;
      case 2:
        // Standard syntax
        document.getElementById("arrow").style.transform = "rotate(90deg)";
        break;
      case 3:
        // Standard syntax
        document.getElementById("arrow").style.transform = "rotate(270deg)";
        break;
    }
}

//Muestra alerta parecida a la de bootstrap al cabo de 200ms
//para detectar cuando eliminar la alerta cuando se clica en cualquier zona del documento
function alert(missatge) {
  setTimeout(function(){
    document.getElementById('text').innerHTML = missatge;
    document.getElementById('alert').style.visibility = "visible";
  }, 200);
}

//Elimina la alerta actual, se usa en el html como método llamado por el onClick del botón de cerrar alerta
function eliminaAlert() {
  document.getElementById('alert').style.visibility = "hidden";
}

//Además, se puede cerrar la alerta clicando fuera de ésta
$(document).click(function(evt) {
  if (document.getElementById('alert').style.visibility === "hidden") {
    return;
  } else {
    eliminaAlert();
  }
});
